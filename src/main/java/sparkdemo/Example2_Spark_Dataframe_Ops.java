package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;

public class Example2_Spark_Dataframe_Ops {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if (Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        } else {
            sparkSession = SparkSession.builder()
                    .master("local[*]")
                    .config("spark.hadoop.validateOutputSpecs", false) // allows overwriting of output
                    .getOrCreate();
        }

        // Note how there is no sparkContext here - we use the sparkSession directly
        Dataset<Row> airports = sparkSession.read().option("header", true).csv("data/airports.csv");

        // How to know the inferred schema of the csv file and display the first few rows?
        airports.printSchema();
        airports.show(5);

        // How to show the number of rows with Country = Papua New Guinea?
        System.out.println("PNG:" + airports.filter("Country = \"Papua New Guinea\"").count());

        // How to print the list of distinct countries
        airports.select("Country").distinct().sort("Country").show();

        // cast latitude and longitude to Double
        airports.col("latitude").cast("Double").as("latitude");
        airports.col("longitude").cast("Double").as("longitude");

        // then filter by -23 < latitude < 23
        Dataset<Row> tropAirports = airports.filter("latitude > -23 AND latitude < 23");

        // these are airports in the tropics - display the results
        tropAirports.show();

        tropAirports.cache();

        // Group tropical airports by timezone and show how many in each zone
        airports.groupBy("timezoneOlsonFormat").count().show();

        // the list of tropical airports could be saved to any supported store - e.g. parquet on hdfs
        tropAirports.write().parquet("data/tropAirports.parquet");
    }
}