package sparkdemo;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Example1_Spark_RDD_Ops {

    // A demonstration function to demonstrate how a custom mapping is just a regular java function
    public static int countWords(final String line) {
        List<String> words = new ArrayList<String>(Arrays.asList(line.split(" ")));
        words.remove("");
        return words.size();
    }

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                                       .master("local[*]")
                                       .config("spark.hadoop.validateOutputSpecs", false)
                                       .getOrCreate();
        }

        // Take a spark context from the spark session
        JavaSparkContext sc = JavaSparkContext.fromSparkContext(sparkSession.sparkContext());

        // Create a Java list and convert it to a spark RDD
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> numbersRdd = sc.parallelize(numbers);

        // Print the number of items in the RDD
        System.out.println(numbersRdd.count());

        //----------------------------------------
        // Now load lines from a text file
        //----------------------------------------
        JavaRDD<String> lines = sc.textFile("data/Macbeth.txt");

        // print the total number of lines
        System.out.println(lines.count());

        // filter to just the lines that contain the word "foul"
        JavaRDD<String> foulLines = lines.filter( line ->  line.toLowerCase().contains("foul") );

        System.out.println("Number of lines containing \"foul\": " + foulLines.count());

        // filter out empty lines, split each on space, count number of words
        // here we're chaining multiple RDD operations together.
        JavaRDD<Integer> numWords = lines.filter(line -> line.trim().length() > 0)
                                         .map(line -> line.split(" "))
                                         .map(words -> words.length);

        // An Alternative to the above, demonstrating using the
        // countWords function that was defined at the top of this file.
        JavaRDD<Integer> numWordsAlt = lines.map(line -> countWords(line));

        // Note: numWords and numWordsAlt should be the same
        // they should BOTH be an RDD of numbers, each number is the count of words on the corresponding line in Macbeth

        // Save RDD to a text file - you could experiment with different numbers of threads/cores/executors
        // e.g. local[1] vs local[2] => notice the different number of output files
        numWords.saveAsTextFile("data/numWords");

        // reduce to the total words - each time the function is called it's parameters will be:
        // the return value from the previous call (prev) and the next item in the RDD (current)
        System.out.println("totalWords:" + numWords.reduce( (prev, current) -> prev + current));

        // Demonstrate a reducer - finding the max words any line
        System.out.println("Max Words in a line:" +
                           numWords.reduce( (prev, current) -> prev > current ? prev : current));

        // Demonstrate a reducer - find the longest line
        System.out.println("Longest Line:\n" +
                           lines.reduce( (prev, current) ->
                                   prev.split(" ").length > current.split(" ").length ? prev : current));
        
        System.out.println("Done");
    }
}
