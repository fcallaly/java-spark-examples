package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;

public class Example6_Spark_Dataframe_Parquet_Read {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                                       .master("local[*]")
                                       .config("spark.hadoop.validateOutputSpecs", false)
                                       .getOrCreate();
        }

        // read data in parquet file(s) from data/airports_parquet directory
        Dataset<Row> tropAirports = sparkSession.read().parquet("data/tropAirports.parquet");

        // show the first 10 rows
        tropAirports.show(10);

        // check the schema?
        tropAirports.printSchema();

        // how many airports did we read?
        System.out.println("Number of airports:" + tropAirports.count());
    }
}
