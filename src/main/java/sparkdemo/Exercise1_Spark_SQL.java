package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;

public class Exercise1_Spark_SQL {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                    .master("local[1]")
                    .config("spark.hadoop.validateOutputSpecs", false)
                    .getOrCreate();
        }

        // just some variable - these would come from configuration or command line arguments
        String jdbcQuery = "select * from city";
        String username = "****";
        String password = "*******";
        String jdbcUrl = "jdbc:mysql://" + username + "@big-data15.conygre.com:3306/world";


        // Option 1. Using the more verbose "load" API
        Dataset<Row> countries = sparkSession.read()
                .format("jdbc")
                .option("driver", "com.mysql.cj.jdbc.Driver")
                .option("url", jdbcUrl)
                .option("username", username)
                .option("password", password)
                .option("query", jdbcQuery)
                .load();


        // 1. What did we just read in?
        // - check the schema - display the first few rows

        // 2. How many distinct entries are in CountryCode?

        // 3. Create a tempView and filter to cities in California USA

        // 4. Get the average city Population per country?

        // 5. Optional - read countries table, and left join the average city population

        // 6. Optional - build a self-contained jar and run on cluster
    }
}