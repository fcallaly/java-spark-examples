package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Properties;

public class Example4_Spark_Dataframe_Jdbc {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                                       .master("local[*]")
                                       .config("spark.hadoop.validateOutputSpecs", false)
                                       .getOrCreate();
        }

        // Some variables to interface with a MySQL database
        // these would come from configuration, properties or command line arguments
        String jdbcQuery = "select * from country where Continent like \"North%\"";
        String username = "****";
        String password = "*******";
        String jdbcUrl = "jdbc:mysql://" + username + "@ms-spark16.conygre.com:3306/world";


        // Option 1. Using the more verbose "load" API
        Dataset<Row> countries = sparkSession.read()
                                    .format("jdbc")
                                    .option("driver", "com.mysql.cj.jdbc.Driver")
                                    .option("url", jdbcUrl)
                                    .option("username", username)
                                    .option("password", password)
                                    .option("query", jdbcQuery)
                                    .load();

        // Option 2. An alternative is to use read().jdbc() - here we read the entire table
        Properties jdbcProps = new Properties();
        jdbcProps.put("password", password);
        //Dataset<Row> countries = sparkSession.read()
        //                        .jdbc(jdbcUrl, "country", jdbcProps);

        // What did we just read in?
        // - check the schema - display the first few rows
        countries.printSchema();
        countries.show(5);

        // Experiment with filtering - e.g. which country has the greatest LifeExpectancy?
        countries.createOrReplaceTempView("countries");
        sparkSession.sql("SELECT * FROM countries WHERE LifeExpectancy = (select MAX(LifeExpectancy) from countries )").show();

        // What's the average GNP per Region?
        Dataset<Row> regionalGnp = countries.groupBy("Region").avg("GNP");
        regionalGnp.show();

        // Save to new database table
        regionalGnp.write()
                .mode(SaveMode.ErrorIfExists)  // this mode will create a new table with this data or else Exception
                .option("driver", "com.mysql.cj.jdbc.Driver")
                .option("url", jdbcUrl)
                .option("username", username)
                .option("password", password)
                .jdbc(jdbcUrl, "Example4RegionalGnp", jdbcProps);

        // join countries and airports
        // read the airports csv
        Dataset<Row> airports = sparkSession.read().option("header", true).csv("data/airports.csv");
        airports.show(5);

        // select Name, Continent, SurfaceArea from countries
        Dataset<Row> trimmedCountries = countries.select("Name", "Continent", "SurfaceArea");

        // join (default type is inner)
        Dataset<Row> joinedDataset = airports.join(trimmedCountries,
                airports.col("Country").equalTo(trimmedCountries.col("Name")));

        // show results
        joinedDataset.show();

        // example of Java join on multiple columns - note we could use SQL to join
        Dataset<Row> joinedDatasetAlt = airports.join(countries,
                (airports.col("Country").equalTo(countries.col("Name")))
                .or
                (airports.col("Country").equalTo(countries.col("LocalName")))
        );

        joinedDatasetAlt.show();
    }
}
