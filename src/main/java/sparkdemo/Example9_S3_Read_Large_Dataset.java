package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;

import static org.apache.spark.sql.functions.desc;

public class Example9_S3_Read_Large_Dataset {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder()
                                       .config("spark.hadoop.fs.s3a.bucket.all.committer.magic.enabled", true)
                                       .getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                    .master("local[*]")
                    .config("spark.hadoop.fs.s3a.bucket.all.committer.magic.enabled", true)
                    .config("spark.hadoop.validateOutputSpecs", false)
                    .getOrCreate();
        }

        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider");
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.access.key", "ACCESS_KEY_HERE");
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.secret.key", "SECRET_KEY_HERE");
        sparkSession.sparkContext()
                .hadoopConfiguration().set("fs.s3a.endpoint", "s3.amazonaws.com");


        // Read Dataset from AWS S3 - this should be 4 csv files that collectively are ~10GB / approx 40M rows
        Dataset<Row> parkingDf = sparkSession.read()
                                             .option("header", true)
                                             .csv("s3a://datasets-22/NYC_Parking_Fines/");

        parkingDf.show();
        System.out.println("Num rows read: " + parkingDf.count());

        // GroupBy and count
        Dataset<Row> outputDf = parkingDf.groupBy("Violation Description").count().orderBy(desc("count"));

        // show for demonstration - don't truncate the column contents
        outputDf.show(false);

        // this can be written back to s3
        outputDf.write().csv("s3a://datasets-22/example9/CountByViolation.csv");
    }
}
