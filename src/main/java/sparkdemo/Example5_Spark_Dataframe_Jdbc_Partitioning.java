package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Properties;

public class Example5_Spark_Dataframe_Jdbc_Partitioning {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                                       .master("local[*]")
                                       .config("spark.hadoop.validateOutputSpecs", false)
                                       .getOrCreate();
        }

        // just some variable - these would come from configuration or command line arguments
        String jdbcQuery = "select * from city";
        String username = "****";
        String password = "*******";
        String jdbcUrl = "jdbc:mysql://" + username + "@ms-spark16.conygre.com:3306/world";


        // Option 1. Using the more verbose "load" API
        Dataset<Row> cities = sparkSession.read()
                                    .format("jdbc")
                                    .option("driver", "com.mysql.cj.jdbc.Driver")
                                    .option("url", jdbcUrl)
                                    .option("lowerBound", 1)
                                    .option("upperBound", 4000)
                                    .option("numpartitions", 2)
                                    .option("partitioncolumn", "ID")
                                    .option("username", username)
                                    .option("password", password)
                                    .option("dbtable", "city")
                                    .load();

        // Option 2. An alternative is to use read().jdbc() - here we read the entire table
        Properties jdbcProps = new Properties();
        jdbcProps.put("password", password);
        //Dataset<Row> countries = sparkSession.read()
        //                        .jdbc(jdbcUrl, "country", jdbcProps);

        // What did we just read in?
        cities.show(5, false);
        System.out.println("Number of records in cities: " + cities.count());

        // Check the schema
        cities.printSchema();

        // What's the average Population per CountryCode?
        // groupBy CountryCode get avg Population
        Dataset<Row> avgGnp = cities.groupBy("CountryCode").avg("Population");
        avgGnp.show();

        // getNumPartitions should result in 2, because numPartitions was set to 2
        // For the two RDDs getNumPartitions
        System.out.println("Num Partitions in cities: " + cities.rdd().getNumPartitions());
        System.out.println("Num Partitions in avgGNP: " + avgGnp.rdd().getNumPartitions());
    }
}
