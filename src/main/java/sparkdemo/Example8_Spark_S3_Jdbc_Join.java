package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Properties;

public class Example8_Spark_S3_Jdbc_Join {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                    .master("local[*]")
                    .config("spark.hadoop.validateOutputSpecs", false)
                    .getOrCreate();
        }

        // Access Key and Secret Key are needed to access S3
        // They should be set as environmental variables or using a credential manager
        // We are showing them here FOR DEMONSTRATION ONLY!
        // See https://hadoop.apache.org/docs/current/hadoop-aws/tools/hadoop-aws/index.html#Authenticating_with_S3
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider");
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.access.key", "ACCESS_KEY_HERE");
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.secret.key", "SECRET_KEY_HERE");
        sparkSession.sparkContext()
                .hadoopConfiguration().set("fs.s3a.endpoint", "s3.amazonaws.com");


        // Read Dataset from AWS S3
        Dataset<Row> covidDf = sparkSession.read().option("header", true)
                .csv("s3a://data-eng-21/datasets/owid-covid-data-250122.csv");

        covidDf.show(5);

        // Read second Dataset from Jdbc
        // just some variable - these would come from configuration or command line arguments
        String jdbcQuery = "select Code, Name, Region, GNP, SurfaceArea from country";
        String username = "****";
        String password = "*******";
        String jdbcUrl = "jdbc:mysql://" + username + "@big-data15.conygre.com:3306/world";

        // Using the more verbose "load" API
        Dataset<Row> countries = sparkSession.read()
                .format("jdbc")
                .option("driver", "com.mysql.cj.jdbc.Driver")
                .option("url", jdbcUrl)
                .option("username", username)
                .option("password", password)
                .option("query", jdbcQuery)
                .load();

        countries.show(5);

        // now join on iso_code
        Dataset<Row> joinedDf = countries.join(covidDf, countries.col("Code").equalTo(covidDf.col("iso_code")), "inner");

        joinedDf.show();

        // this could be written back to s3
        joinedDf.write().csv("s3a://data-eng-21/datasets/example8/joined-owid-covid-250122.csv");

        // or alternatively written to a RDBMS
        // just some variable - these would come from configuration or command line arguments
        Properties jdbcProps = new Properties();
        jdbcProps.put("password", password);

        joinedDf.write()
                .mode(SaveMode.ErrorIfExists)
                .option("driver", "com.mysql.cj.jdbc.Driver")
                .option("url", jdbcUrl)
                .option("username", username)
                .option("password", password)
                .jdbc(jdbcUrl, "Example8CovidData", jdbcProps);
    }
}
