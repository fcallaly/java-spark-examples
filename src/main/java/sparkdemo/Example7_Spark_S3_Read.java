package sparkdemo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;

public class Example7_Spark_S3_Read {

    public static void main(String[] args) {
        SparkSession sparkSession;

        if(Arrays.asList(args).contains("--cluster")) {
            sparkSession = SparkSession.builder().getOrCreate();
        }
        else {
            sparkSession = SparkSession.builder()
                    .master("local[*]")
                    .config("spark.hadoop.validateOutputSpecs", false)
                    .getOrCreate();
        }

        // Access Key and Secret Key are needed to access S3
        // They should be set as environmental variables or using a credential manager
        // We are showing them here FOR DEMONSTRATION ONLY!
        // See https://hadoop.apache.org/docs/current/hadoop-aws/tools/hadoop-aws/index.html#Authenticating_with_S3
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider");
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.access.key", "ACCESS_KEY_HERE");
        sparkSession.sparkContext().hadoopConfiguration()
                .set("fs.s3a.secret.key", "SECRET_KEY_HERE");
        sparkSession.sparkContext()
                .hadoopConfiguration().set("fs.s3a.endpoint", "s3.amazonaws.com");

        // Note how there is no sparkContext here - we use the sparkSession directly
        // when the appropriate libraries are loaded, we can access S3 in the same way as HDFS
        Dataset<Row> covidDf = sparkSession.read()
                                           .option("header", true)
                                           .csv("s3a://data-eng-21/datasets/owid-covid-data-250122.csv");

        covidDf.show(5);
    }
}
