This is a collection of Java examples and exercises using Spark.

The project will build as a single self-contained jar that can be run with spark-submit and referencing a specific class.

e.g.

```spark-submit --master spark://spark-master:7077 --class sparkdemo.Example8_Spark_S3_Jdbc_Join javaSpark-1.0-SNAPSHOT-jar-with-dependencies.jar --cluster ```